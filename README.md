# Spotify Top 100 in 2018

From an open source I found on Kaggle. Based on many inspiring data analysis reports by data scietists, data analysts and students, I was motivated to try learn to make the trend analysis. 
Resource: https://www.kaggle.com/alexbonella/complete-analysis-top-100-spotify-tracks-of-2018

### This analysis includes
- Analysing top artists by the number of artist's song lists on Top 100
- Comparison of similarities amongst different categories using heatmap
- Futher analysis of individual category e.g. danceability, energy, loudness
 
